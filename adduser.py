#!/usr/bin/python2
import boto3
import sys

def main():
    try:
        print 
        client = boto3.client('iam',aws_access_key_id=sys.argv[1],aws_secret_access_key=sys.argv[2])
    except:
        print "usage python2 addusers.py 'access key' 'secret access key'"
        exit(0)
    users = client.list_users()
    user_list = []
    for key in users['Users']:
    	result = {}
    	result['userName']=key['UserName']
        print "Checking User : "+ key['UserName']
    	List_of_MFA_Devices = client.list_mfa_devices(UserName=key['UserName'])

        if (List_of_MFA_Devices['MFADevices']) is None:
	    user_list.append(result)    	
#    print user_list   		
    for nmfa_user in user_list:
        print "Adding user : " + nmfa_user['userName'] + "to AllUsers"
    	response = client.add_user_to_group(GroupName = "Allusers", UserName = nmfa_user['userName'])

if __name__ == "__main__":
    main()
